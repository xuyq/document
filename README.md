# 文档库

个人文档图片资料仓库，保存公开数据，如书法、原神、随笔等等。


仓库：[bitbucket]( https://bitbucket.org/xu12345/document ) &ensp; [gitcode]( https://gitcode.net/xu180/document ) &ensp; [github]( https://github.com/scott180/document-article.git ) &ensp; [sourceforge]( https://sourceforge.net/p/xdocument/code/ci/master/tree/ )


书法练习轨迹：[pdf文件]( https://gitcode.net/xu180/document/-/tree/master/pdf ) &ensp; [ReadMe]( https://xyqin.coding.net/public/my/calligraphy/git/files )


原神：[图片]( https://bitbucket.org/xu12345/document/src/master/imgs/yuanshen/ ) &ensp; [练度]( https://sourceforge.net/p/xdocument/code/ci/master/tree/imgs/yuanshen/%E6%88%91%E7%9A%84/20230909/ ) &ensp; [我的原神旅行观测记录]( https://gitcode.net/xu180/document/-/blob/master/article/%E9%9A%8F%E7%AC%94/%E6%88%91%E7%9A%84%E5%8E%9F%E7%A5%9E%E6%97%85%E8%A1%8C%E8%A7%82%E6%B5%8B%E8%AE%B0%E5%BD%95.md )

<br>

---


我的网站：生活随笔-编程笔记-书法练习轨迹


[xushufa]( https://xushufa.cn ) &ensp; [blog]( https://vuepress-blog.xushufa.cn )

***




